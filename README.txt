CROSP ANDROID APP

Versão 1.1
	Código de ética:
		- Feito o botão "Código de Ética do Menu" baixar e abrir o código de ética;
		- Atualizado o código de ética para o novo enviado;
		- Alterada a lógica do código de ética para sempre efetuar o download quando solicitado.

	Notícias:
		- Criado recurso de zoom para as notícias;
		- Corrigido erro de notícias que não abriam em algumas versões mais antigas do android.
	
	Boletins Periódicos:
		- Corrigido problema que impedia de abrir boletins periódicos que não possuíam http:// em seu endereço (Ex: Edição 13).
	
	Fale Conosco:
		- Adicionado * para indicar os campos obrigatórios.
		
	Sobre o Desenvolvimento:
		- Removidas informações sobre a HiMaker, deixada a mensagem "Desenvolvido por" e a logo e nome da empresa que abre o site da HiMaker.

	Tela Inicial:
		- Criada tela inicial que apresenta a logo do CROSP.	
		
	Sobre o CROSP:
		- Links para as redes sociais e para o site do CROSP.
		
	Inscrições
		- Adicionados links para inscrições de Programa de Integracao e Palestras SEBRAE.
		
		

Versão 1.0
	- Notícias;
	- CROSP em Notícia;
	- Publicações;
	- Fale Conosco;
	- Código de Ética;
	- O CROSP.

Problemas conhecidos
 1. Se fizer o movimento para consultar notícias mais antigas e não houver mais notícias, a imagem de carregando não desaparece;
 2. Em algumas versões do android o jornal (testado na 2.3.3 e na 4.4.2), quando acessado, não é apresentado com zoom de 100%.

Para próximas versões
 1. Adicionar informação dos jornais já baixados e dos que precisam de download;
 2. Adicionar opção de cancelar o download do jornal;
 3. Adicionar opção de remover jornal baixado;
 4. Salvar jornais em pasta que eh removida quando a aplicação é removida;
 5. Fazer os jornais e boletim periódicos aparecerem como submenu de Publicações no menu;
 6. Adicionar seccionais. 

Dúvidas com professor:
 1. Como fazer um link de uma activity para outra activity, porém fazendo carregar um fragmento(Ex: link voltar da tela da HiMaker);
 2. Como apresentar submenu no menu do app;
 3. Como fazer para o boletim aparecer 100% ao apresenta-lo na versao 2.3.3;
 4. Como salvar arquivo na pasta do app e deixá-lo disponível para acesso externo;
 5. Como, no componente spinner, apresentar mensagem de "Escolha uma opção" quando nenhuma foi selecionada.