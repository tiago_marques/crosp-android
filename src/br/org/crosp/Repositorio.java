package br.org.crosp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Repositorio extends Database {

	private static final String NOME = "crosp.db";
	private static final int VERSAO = 1;

	public Repositorio(Context context) {
		super(context, NOME, VERSAO);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE noticia (id INTEGER PRIMARY KEY AUTOINCREMENT, titulo TEXT, data INTEGER, imagem_destaque BLOB, imagem BLOB, conteudo TEXT)");
		// db.execSQL("CREATE TABLE noticia_imagem (id INTEGER PRIMARY KEY AUTOINCREMENT, id_noticia INTEGER, legenda TEXT, data INTEGER, imagem BLOB)");
		db.execSQL("CREATE TABLE publicacao (id INTEGER PRIMARY KEY AUTOINCREMENT, titulo TEXT, data INTEGER, categoria INTEGER)");
		db.execSQL("CREATE TABLE jornal (id INTEGER PRIMARY KEY, arquivo TEXT, FOREIGN KEY (id) REFERENCES publicacao(id))");
		db.execSQL("CREATE TABLE boletim (id INTEGER PRIMARY KEY, link TEXT, FOREIGN KEY (id) REFERENCES publicacao(id))");
		onUpgrade(db, 0, db.getVersion());

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		switch (oldVersion) {
		case 1:
		default:
			break;
		}
	}

}
