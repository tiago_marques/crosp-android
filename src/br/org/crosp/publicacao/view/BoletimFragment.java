package br.org.crosp.publicacao.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import br.org.crosp.BreadCrumb;
import br.org.crosp.noticia.R;

public class BoletimFragment extends Fragment {

	private View rootView;

	@SuppressLint("NewApi")
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.boletim_fragment, container, false);

		String url = getArguments().getString("url");

		loadURL(url);

		return rootView;
	}

	protected void loadURL(String url) {

		WebView webView = (WebView) rootView.findViewById(R.id.conteudoboletim);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.setInitialScale(100);
		webView.getSettings().setBuiltInZoomControls(true);


		webView.loadUrl(adicionarHTTP(url));
	}

	@Override
	public void onResume() {
		getActivity().setTitle("Boletim Peri�dico");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);

		super.onResume();
	}

	/**
	 * Para o webView conseguir encontrar a p�gina onde se encontra o boletim o endere�o deve conter http://
	 * 
	 * @see https://teamtreehouse.com/forum/url-not-loading-in-webview
	 * @return Caso a url informada n�o contenha o sufixo http://, concatena, caso contr�rio a string n�o � modificada.
	 */
	public String adicionarHTTP(String url) {

		String sufixo = url.substring(0, 7);
		return sufixo.equals("http://") ? url : "http://".concat(url);
	}

}
