package br.org.crosp.publicacao.view;

import java.util.Collections;
import java.util.LinkedList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import br.org.crosp.BreadCrumb;
import br.org.crosp.noticia.R;
import br.org.crosp.publicacao.model.BoletimPeriodico;
import br.org.crosp.publicacao.service.BoletimPeriodicoService;

import com.costum.android.widget.PullToRefreshListView;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;

public class BoletinsFragment extends Fragment {

	private BoletimPeriodicoService boletimService;
	private PullToRefreshListView list;
	private BoletimAdapter boletimAdapter;
	private Context ctx;
	private LinkedList<BoletimPeriodico> boletins;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.lista_boletim_fragment,
				container, false);

		boletimService = new BoletimPeriodicoService(rootView.getContext());
		list = (PullToRefreshListView) rootView
				.findViewById(R.id.lista_boletins);
		ctx = rootView.getContext();

		LoadUltimosBoletins load = new LoadUltimosBoletins();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			load.execute();
		}

		return rootView;
	}

	/**
	 * Tarefa assincrona para consultar os �ltimos {@link BoletimPeriodico}.
	 * Classe que monta a lista de {@link BoletimPeriodico} pela primeira vez.
	 */
	private class LoadUltimosBoletins extends
			AsyncTask<Void, Void, LinkedList<BoletimPeriodico>> {

		@Override
		protected LinkedList<BoletimPeriodico> doInBackground(Void... params) {

			boletins = new LinkedList<BoletimPeriodico>(
					boletimService.ultimosBoletins());
			Collections.sort(boletins);
			return boletins;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(LinkedList<BoletimPeriodico> result) {

			boletimAdapter = new BoletimAdapter(ctx, result);
			list.setAdapter(boletimAdapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					BoletimPeriodico boletim = (BoletimPeriodico) parent
							.getAdapter().getItem(position);

					Bundle bundle = new Bundle();
					bundle.putString("url", boletim.getLink());

					BoletimFragment boletimFragment = new BoletimFragment();
					boletimFragment.setArguments(bundle);

					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.content_frame, boletimFragment)
							.commit();

				}
			});

			// adiciona acao para dar refresh na lista
			list.setOnRefreshListener(new OnRefreshListener() {
				public void onRefresh() {

					RefreshBoletim refresh = new RefreshBoletim();
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						refresh.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					} else {
						refresh.execute();
					}
				}
			});

			super.onPostExecute(result);
		}
	}

	/**
	 * Tarefa para fazer consultar se existe novos {@link BoletimPeriodico}.
	 */
	private class RefreshBoletim extends
			AsyncTask<Void, Void, LinkedList<BoletimPeriodico>> {

		@Override
		protected LinkedList<BoletimPeriodico> doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}

			return new LinkedList<BoletimPeriodico>(boletimService.refresh());
		}

		@Override
		protected void onPostExecute(LinkedList<BoletimPeriodico> result) {

			boletins.addAll(0, result);
			boletimAdapter.notifyDataSetChanged();
			list.onRefreshComplete();
			super.onPostExecute(boletins);
		}

		@Override
		protected void onCancelled() {
			list.onRefreshComplete();
		}
	}


	@Override
	public void onResume() {
		getActivity().setTitle("Boletim Peri�dico");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);
		super.onResume();
	}

}
