package br.org.crosp.publicacao.view;

import java.util.Collections;
import java.util.LinkedList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import br.org.crosp.BaixarAppFragment;
import br.org.crosp.BreadCrumb;
import br.org.crosp.Util;
import br.org.crosp.noticia.R;
import br.org.crosp.publicacao.model.Jornal;
import br.org.crosp.publicacao.service.JornalService;

import com.costum.android.widget.PullToRefreshListView;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;

public class JornaisFragment extends Fragment {

	private JornalService jornalService;
	private PullToRefreshListView list;
	private JornalAdapter jornalAdapter;
	private Context ctx;
	private LinkedList<Jornal> jornais;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.lista_jornal_fragment,
				container, false);

		jornalService = new JornalService(rootView.getContext());
		list = (PullToRefreshListView) rootView
				.findViewById(R.id.lista_jornais);
		ctx = rootView.getContext();

		LoadUltimosJornais load = new LoadUltimosJornais();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			load.execute();
		}

		return rootView;
	}

	/**
	 * Tarefa assincrona para consultar os �ltimos {@link Jornal}.
	 * Classe que monta a lista de {@link Jornal} pela primeira vez.
	 */
	private class LoadUltimosJornais extends
			AsyncTask<Void, Void, LinkedList<Jornal>> {

		@Override
		protected LinkedList<Jornal> doInBackground(Void... params) {

			jornais = new LinkedList<Jornal>(jornalService.ultimosJornais());
			Collections.sort(jornais);
			return jornais;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(LinkedList<Jornal> result) {

			jornalAdapter = new JornalAdapter(ctx, result);
			list.setAdapter(jornalAdapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					Jornal jornal = (Jornal) parent.getAdapter().getItem(
							position);

					String url = JornalService.PDF_PATH + jornal.getArquivo();
					
					if (!Util.canDisplay(ctx, "application/pdf")) {
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager
								.beginTransaction()
								.replace(R.id.content_frame,
										new BaixarAppFragment()).commit();
					} else {
						Util.downloadNotification(ctx, url,
								jornal.getArquivo(), "application/pdf",
								jornal.getTitulo(), null);
					}
				}
			});

			// adiciona acao para dar refresh na lista
			list.setOnRefreshListener(new OnRefreshListener() {
				public void onRefresh() {

					RefreshJornais refresh = new RefreshJornais();
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						refresh.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					} else {
						refresh.execute();
					}
				}
			});

			super.onPostExecute(result);
		}
	}

	/**
	 * Tarefa para fazer consultar se existe novos jornais.
	 */
	private class RefreshJornais extends
			AsyncTask<Void, Void, LinkedList<Jornal>> {

		@Override
		protected LinkedList<Jornal> doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}

			return new LinkedList<Jornal>(jornalService.refresh());
		}

		@Override
		protected void onPostExecute(LinkedList<Jornal> result) {

			jornais.addAll(0, result);
			jornalAdapter.notifyDataSetChanged();
			list.onRefreshComplete();
			super.onPostExecute(jornais);
		}

		@Override
		protected void onCancelled() {
			list.onRefreshComplete();
		}
	}


	@Override
	public void onResume() {
		getActivity().setTitle("Jornais");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);
		super.onResume();
	}

}
