package br.org.crosp.publicacao.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.org.crosp.noticia.R;
import br.org.crosp.publicacao.model.Jornal;

public class JornalAdapter extends ArrayAdapter<Jornal> {

	private static final int layoutResourceId = R.layout.jornal_item;
	private LayoutInflater inflater;

	public JornalAdapter(Context context, List<Jornal> objects) {
		super(context, layoutResourceId, objects);
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		convertView = inflater.inflate(layoutResourceId, null);

		TextView texto = (TextView) convertView
				.findViewById(R.id.titulochamada);
		TextView data = (TextView) convertView.findViewById(R.id.datajornal);

		Jornal j = getItem(position);

		texto.setText(j.getTitulo());

		Date dataa = j.getData();
		if (dataa != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:mm");
			data.setText(sdf.format(dataa));
		}

		return convertView;
	}

}
