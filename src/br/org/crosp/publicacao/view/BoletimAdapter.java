package br.org.crosp.publicacao.view;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.org.crosp.noticia.R;
import br.org.crosp.publicacao.model.BoletimPeriodico;

public class BoletimAdapter extends ArrayAdapter<BoletimPeriodico> {

	private static final int layoutResourceId = R.layout.boletim_item;
	private LayoutInflater inflater;

	public BoletimAdapter(Context context, List<BoletimPeriodico> objects) {
		super(context, layoutResourceId, objects);
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		convertView = inflater.inflate(layoutResourceId, null);

		TextView texto = (TextView) convertView
				.findViewById(R.id.tituloboletim);

		BoletimPeriodico boletim = getItem(position);

		texto.setText(boletim.getTitulo());

		return convertView;
	}

}
