package br.org.crosp.publicacao.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import br.org.crosp.Reader;
import br.org.crosp.Util;
import br.org.crosp.noticia.service.NoticiaService;
import br.org.crosp.publicacao.model.Jornal;

/**
 * Servi��es para as pubica��es de {@link Jornal}.
 * 
 * @author Tiago Wanke Marques <dev08@himaker.com.br>
 */
public class JornalService extends PublicacaoService<Jornal> {

	public static final String PDF_PATH = "http://www.crosp.org.br/uploads/publicacoes/";

	public JornalService(Context ctx) {
		super(ctx, Jornal.class);
	}

	public File downloadJornal(String arquivoPDF) {

		FileOutputStream fos;
		File retorno = null;
		try {

			retorno = File
					.createTempFile(arquivoPDF, ".pdf",
					ctx.getExternalCacheDir());

			fos = new FileOutputStream(retorno);
			fos.write(Util.download(PDF_PATH + arquivoPDF));
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return retorno;
	}

	/**
	 * Retorna os ultimos {@link Jornal} cadastrados no site do CROSP, seguindo a seguinte regra.
	 * 
	 * 1. Consulta novos no site se n�o houver no banco, caso
	 * contr�rio retorna os cadastrados no banco
	 * 
	 * 2. Quando n�o houver no banco, consulta os ultimos cadastrados
	 * no site, no m�ximo ( {@link PublicacaoService#LIMIT} )
	 * 		2.1 Adiciona os contultados do site no banco local
	 * 		2.2 Consulta no banco local e retorna.
	 */
	public List<Jornal> ultimosJornais() {

		// se n�o houver cadastrado no banco local
		if (qtdPublicacoes() == 0) {
			// consulta no site
			List<Jornal> jornais = jsonJornaisToJornais();

			// adicionano banco local
			insert(jornais);
		}

		// consulta no banco local e retorna
		return jornaisFromBanco();
	}

	/**
	 * Consulta a lista de {@link Jornal} no site do crosp que possuem o id maior que o id do ultimo jornal
	 * cadastrado no banco local. Adiciona as noticias consultadas no banco local e as retorna.
	 */
	public List<Jornal> refresh() {

		// consulta jornais no site crosp
		List<Jornal> jornal = jornaisNovosFromJSON(ultimaPublicacao());

		// adiciona as jornais no banco local
		insert(jornal);
		return jornal;
	}

	/**
	 * Consulta os jornais no site e retorna em objeto {@link Jornal}.
	 * 
	 * @param json
	 */
	private List<Jornal> jsonJornaisToJornais() {

		List<Jornal> jornais = new ArrayList<Jornal>();

		JSONArray jArray = publiacaoesFromJSON();
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				try {
					jornais.add(jsonJornalToJornal(jArray.getJSONObject(i)));
				} catch (JSONException e) {
					Log.e(this.getClass().getSimpleName(),
							"Erro ao tentar carregar jornal do site CROSP.", e);
				}
			}
		}

		return jornais;
	}

	/**
	 * @param idJornal
	 * @return Lista de {@link Jornal} consultada no site do crosp com id maior que o informado.
	 */
	private List<Jornal> jornaisNovosFromJSON(int idJornal) {

		List<Jornal> jornais = new ArrayList<Jornal>();
		JSONArray jArray = publicacoesNovasFromJSON(idJornal);
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				try {
					jornais.add(jsonJornalToJornal(jArray.getJSONObject(i)));
				} catch (JSONException e) {
					Log.e(this.getClass().getSimpleName(),
							"Erro ao tentar carregar novos jornais do site do CROSP.",
							e);
				}
			}
		}

		return jornais;
	}

	/**
	 * Transforma um ojeto {@link JSONObject} retornado pelo servico
	 * {@link Util#consultaJSON(String)} para objeto {@link Jornal}
	 * 
	 * @param json
	 */
	private Jornal jsonJornalToJornal(JSONObject json) {

		Jornal jornal = jsonPublicacaoToPublicacao(json);

		try {

			if (!json.isNull("arquivo")) {
				jornal.setArquivo(json.getString("arquivo"));
			}

		} catch (JSONException e) {
			Log.e(NoticiaService.class.getSimpleName(),
					"Problema ao carregar a noticia do site." + e.getMessage());
		}

		return jornal;
	}

	/**
	 * Insere os dados dos {@link Jornal} informados na tabela de jornal e publicacao.
	 * 
	 * @see PublicacaoService#insert(br.org.crosp.publicacao.model.Publicacao)
	 * @param jornais
	 */
	private void insert(List<Jornal> jornais) {

		db.open();
		String query = "INSERT INTO jornal (id, arquivo) VALUES (?, ?)";

		for (Jornal jornal : jornais) {
			try {
				super.insert(jornal);
				SQLiteStatement prepStmt = db.getWritableDatabase()
						.compileStatement(query);

				prepStmt.bindLong(1, jornal.getId());
				prepStmt.bindString(2, jornal.getArquivo());
				prepStmt.execute();

			} catch (Exception e) {
				Log.e(this.getClass().getSimpleName(),
						"Problema ao tentar inserir jornal:  " + e);
			}
		}

		db.close();
	}

	/**
	 * @return Lista de {@link Jornal} cadastrados no banco.
	 */
	private List<Jornal> jornaisFromBanco() {

		db.open();
		Reader reader = db
				.query("SELECT * FROM jornal j JOIN publicacao p ON (j.id = p.id)");
		List<Jornal> jornais = new ArrayList<Jornal>();
		while (reader.read()) {
			// jornais.add(toNoticia(reader));
			jornais.add(toJornal(reader));
		}

		db.close();
		// Collections.sort(jornais);
		return jornais;
	}

	private Jornal toJornal(Reader reader) {

		Jornal jornal = toPublicacao(reader);
		jornal.setArquivo(reader.getString("arquivo"));
		return jornal;
	}

}
