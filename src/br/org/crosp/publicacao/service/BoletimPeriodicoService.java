package br.org.crosp.publicacao.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import br.org.crosp.Reader;
import br.org.crosp.Util;
import br.org.crosp.publicacao.model.BoletimPeriodico;

/**
 * Servi��es para as pubica��es de {@link BoletimPeriodico}.
 * 
 * @author Tiago Wanke Marques <dev08@himaker.com.br>
 */
public class BoletimPeriodicoService extends
		PublicacaoService<BoletimPeriodico> {

	public static final String PDF_PATH = "http://www.crosp.org.br/uploads/publicacoes/";

	public BoletimPeriodicoService(Context ctx) {
		super(ctx, BoletimPeriodico.class);
	}

	/**
	 * Retorna os ultimos {@link BoletimPeriodico} cadastrados no site do CROSP, seguindo a seguinte regra.
	 * 
	 * 1. Consulta novos no site se n�o houver no banco, caso
	 * contr�rio retorna os cadastrados no banco
	 * 
	 * 2. Quando n�o houver no banco, consulta os ultimos cadastrados
	 * no site, no m�ximo ( {@link PublicacaoService#LIMIT} )
	 * 		2.1 Adiciona os contultados do site no banco local
	 * 		2.2 Consulta no banco local e retorna.
	 */
	public List<BoletimPeriodico> ultimosBoletins() {

		// se n�o houver cadastrado no banco local
		if (qtdPublicacoes() == 0) {
			// consulta no site
			List<BoletimPeriodico> boletins = jsonBoletinsToBoletins();

			// adicionano banco local
			insert(boletins);
		}

		// consulta no banco local e retorna
		return boletinsFromBanco();
	}

	/**
	 * Consulta a lista de {@link BoletimPeriodico} no site do crosp que possuem o id maior que o id do ultimo 
	 * cadastrado no banco local. Adiciona os consultados no banco local e as retorna.
	 */
	public List<BoletimPeriodico> refresh() {

		// consulta no site crosp
		List<BoletimPeriodico> boletim = boletinsNovosFromJSON(ultimaPublicacao());

		// adiciona no banco local
		insert(boletim);
		return boletim;
	}

	/**
	 * Consulta os {@link BoletimPeriodico} no site e retorna.
	 * 
	 * @param json
	 */
	private List<BoletimPeriodico> jsonBoletinsToBoletins() {

		List<BoletimPeriodico> boletins = new ArrayList<BoletimPeriodico>();

		JSONArray jArray = publiacaoesFromJSON();
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				try {
					boletins.add(jsonBoletimToBoletim(jArray.getJSONObject(i)));
				} catch (JSONException e) {
					Log.e(this.getClass().getSimpleName(),
							"Erro ao tentar carregar boletim peri�dico do site CROSP.",
							e);
				}
			}
		}

		return boletins;
	}

	/**
	 * @param idBoletim
	 * @return Lista de {@link BoletimPeriodico} consultada no site do crosp com id maior que o informado.
	 */
	private List<BoletimPeriodico> boletinsNovosFromJSON(int idBoletim) {

		List<BoletimPeriodico> boletins = new ArrayList<BoletimPeriodico>();
		JSONArray jArray = publicacoesNovasFromJSON(idBoletim);
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				try {
					boletins.add(jsonBoletimToBoletim(jArray.getJSONObject(i)));
				} catch (JSONException e) {
					Log.e(this.getClass().getSimpleName(),
							"Erro ao tentar carregar novos boletins do site do CROSP.",
							e);
				}
			}
		}

		return boletins;
	}

	/**
	 * Transforma um ojeto {@link JSONObject} retornado pelo servico
	 * {@link Util#consultaJSON(String)} para objeto {@link BoletimPeriodico}
	 * 
	 * @param json
	 */
	private BoletimPeriodico jsonBoletimToBoletim(JSONObject json) {

		BoletimPeriodico boletim = jsonPublicacaoToPublicacao(json);

		try {

			if (!json.isNull("link")) {
				boletim.setLink(json.getString("link"));
			}

		} catch (JSONException e) {
			Log.e(getClass().getSimpleName(),
					"Problema ao carregar a boletim do site." + e.getMessage());
		}

		return boletim;
	}

	/**
	 * Insere os dados dos {@link BoletimPeriodico} informados na tabela de boltim e publicacao.
	 * 
	 * @see PublicacaoService#insert(br.org.crosp.publicacao.model.Publicacao)
	 * @param boletins
	 */
	private void insert(List<BoletimPeriodico> boletins) {

		db.open();
		String query = "INSERT INTO boletim (id, link) VALUES (?, ?)";

		for (BoletimPeriodico boletim : boletins) {
			try {
				super.insert(boletim);
				SQLiteStatement prepStmt = db.getWritableDatabase()
						.compileStatement(query);

				prepStmt.bindLong(1, boletim.getId());
				prepStmt.bindString(2, boletim.getLink());
				prepStmt.execute();

			} catch (Exception e) {
				Log.e(this.getClass().getSimpleName(),
						"Problema ao tentar inserir boletim:  " + e);
			}
		}

		db.close();
	}

	/**
	 * @return Lista de {@link BoletimPeriodico} cadastrados no banco.
	 */
	private List<BoletimPeriodico> boletinsFromBanco() {

		db.open();
		Reader reader = db
				.query("SELECT * FROM boletim b JOIN publicacao p ON (b.id = p.id)");
		List<BoletimPeriodico> boletins = new ArrayList<BoletimPeriodico>();
		while (reader.read()) {
			boletins.add(toBoletim(reader));
		}

		db.close();
		return boletins;
	}

	private BoletimPeriodico toBoletim(Reader reader) {

		BoletimPeriodico boletim = toPublicacao(reader);
		boletim.setLink(reader.getString("link"));
		return boletim;
	}

}
