package br.org.crosp.publicacao.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import br.org.crosp.Reader;
import br.org.crosp.Repositorio;
import br.org.crosp.Util;
import br.org.crosp.noticia.service.NoticiaService;
import br.org.crosp.publicacao.model.CategoriaPublicacao;
import br.org.crosp.publicacao.model.Publicacao;

/**
 * Utilizado pelas classes de servi�o de cada {@link CategoriaPublicacao}.
 * Esta classe trabalha com tipo gen�rico, pois a mesma sabe que existem {@link CategoriaPublicacao}, por�m
 * ela n�o deve saber quais as categorias, dessa forma quando um servi�o de uma {@link CategoriaPublicacao}
 * extender essa classe deve informar qual a classe que extende {@link Publicacao} est� se referindo.
 * 
 * @author Tiago Wanke Marques <dev08@himaker.com.br>
 * @param <T> Tipo de {@link Publicacao}.
 */
abstract class PublicacaoService<T extends Publicacao> {

	protected Context ctx;
	protected Repositorio db;

	private Class<T> clazz;

	PublicacaoService(Context ctx, Class<T> clazz) {
		this.ctx = ctx;
		this.db = new Repositorio(ctx);
		this.clazz = clazz;
	}

	/**
	 * Fun��es que precisam de uma instancia de {@link Publicacao} instancia de acordo com a categoria da {@link Publicacao}.
	 * @return
	 */
	private T getPublicacaoObject() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			Log.e(this.getClass().getSimpleName(),
					"Erro ao tentar instanciar Publicacao", e);
		} catch (IllegalAccessException e) {
			Log.e(this.getClass().getSimpleName(),
					"Erro ao tentar instanciar Publicacao pois nao possui default construtor",
					e);
		}
		return null;
	}

	/**
	 * @return Quantidade de {@link Publicacao} cadastradas (da {@link CategoriaPublicacao} informada)
	 * no banco local.
	 */
	protected int qtdPublicacoes() {

		db.open();
		int qtd;
		qtd = db.scalarInt(
				"SELECT COUNT(*) FROM publicacao WHERE categoria = %d",
				getPublicacaoObject().getCategoria().getValor());
		db.close();
		return qtd;
	}

	/**
	 * @return {@link JSONArray} das {@link Publicacao} consultadas no site, 
	 * o limite consultado eh definido {@link PublicacaoService#LIMIT}.
	 */
	protected JSONArray publiacaoesFromJSON() {

		return Util.consultaJSON(Util.URL_SERVICE
				+ "publicacoesByLimit.php?categoria="
				+ getPublicacaoObject().getCategoria().getValor());
	}

	/**
	 * @return {@link JSONArray} das {@link Publicacao} consultadas no site com id maior do id informado da categoria informada (generics).
	 */
	protected JSONArray publicacoesNovasFromJSON(int idPublicacao) {

		return Util.consultaJSON(Util.URL_SERVICE
				+ "publicacoesByLimit.php?idUltima=" + idPublicacao
				+ "&categoria="
				+ getPublicacaoObject().getCategoria().getValor());
	}

	/**
	 * Transforma um ojeto {@link JSONObject} retornado pelo servico
	 * {@link Util#consultaJSON(String)} para objeto {@link Publicacao}
	 * 
	 * @param json
	 */
	protected T jsonPublicacaoToPublicacao(JSONObject json) {

		T publicacao = getPublicacaoObject();
		try {
			publicacao.setId(json.getInt("id"));

			if (!json.isNull("titulo")) {
				publicacao.setTitulo(json.getString("titulo"));
			}

			if (!json.isNull("data")) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd H:mm:ss");
					publicacao.setData(sdf.parse(json.getString("data")));
				} catch (ParseException e) {
					Log.w("Data not�cia",
							"Data da not�cia n�o est� no formato correto (yyyy-MM-dd H:mm:ss)");
				}
			}
		} catch (JSONException e) {
			Log.e(NoticiaService.class.getSimpleName(),
					"Problema ao carregar a noticia do site." + e.getMessage());
		}

		return publicacao;
	}

	/**
	 * Adiciona no banco a {@link Publicacao} informada.
	 * 
	 * @param publicacao
	 */
	protected void insert(T publicacao) {

		db.open();
		String query = "INSERT INTO publicacao (id, titulo, data, categoria) VALUES (?, ?, ?, ?)";

		SQLiteStatement prepStmt = db.getWritableDatabase().compileStatement(
				query);

		try {
			prepStmt.bindLong(1, publicacao.getId());
			prepStmt.bindString(2, publicacao.getTitulo());
			prepStmt.bindLong(3, publicacao.getData().getTime());
			prepStmt.bindLong(4, publicacao.getCategoria().getValor());
			prepStmt.execute();
		} catch (Exception e) {
			Log.e(this.getClass().getSimpleName(),
					"Problema ao tentar inserir publica��o:  " + e);
		}
		db.close();
	}

	protected T toPublicacao(Reader reader) {

		T publicacao = getPublicacaoObject();
		publicacao.setId(reader.getInt("id"));
		publicacao.setTitulo(reader.getString("titulo"));
		publicacao.setData(new Date(reader.getLong("data")));
		return publicacao;
	}

	/**
	 * 
	 * @return Identificador da �ltima (mais recente) {@link Publicacao} cadastrada da {@link CategoriaPublicacao} informada durante a instanciacao do servico.
	 */
	protected int ultimaPublicacao() {

		db.open();
		String query = String.format(
				"SELECT MAX(id) FROM publicacao WHERE categoria = %d",
				getPublicacaoObject().getCategoria().getValor());
		int id = db.scalarInt(query);
		db.close();
		return id;
	}
}
