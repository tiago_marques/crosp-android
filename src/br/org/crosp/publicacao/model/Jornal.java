package br.org.crosp.publicacao.model;


public class Jornal extends Publicacao {

	private String arquivo;

	public Jornal() {
		categoria = CategoriaPublicacao.JORNAL;
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

}
