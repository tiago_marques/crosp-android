package br.org.crosp.publicacao.model;

public class BoletimPeriodico extends Publicacao {

	private String link;

	public BoletimPeriodico() {
		categoria = CategoriaPublicacao.BOLETIM_PERIODICO;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
