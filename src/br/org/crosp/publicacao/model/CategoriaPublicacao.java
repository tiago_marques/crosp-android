package br.org.crosp.publicacao.model;

public enum CategoriaPublicacao {

	JORNAL(1, "Jornal"), BOLETIM_PERIODICO(2, "Boletim Periódico");

	private int valor;
	private String descricao;

	private CategoriaPublicacao(int valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public int getValor() {
		return valor;
	}

	public String toString() {
		return descricao;
	}

}
