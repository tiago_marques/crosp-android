package br.org.crosp.publicacao.model;

import java.util.Date;

public abstract class Publicacao implements Comparable<Publicacao> {

	private int id;
	private String titulo;
	private Date data;
	protected CategoriaPublicacao categoria;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public CategoriaPublicacao getCategoria() {
		return categoria;
	}

	@Override
	public int compareTo(Publicacao another) {
		return another.getData().compareTo(this.getData());
	}
	
}
