package br.org.crosp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public final class Util {

	/**
	 * URL onde est�o os servi�s utilizados pelo app.
	 */
	public static final String URL_SERVICE = "http://www.crosp.org.br/webservice/";

	/**
	 * URL onde est� o arquivo pdf do cc�digo de �tica.
	 */
	public static final String URL_CODIGO_ETICA = "http://www.crosp.org.br/uploads/etica/6ac4d2e1ab8cf02b189238519d74fd45.pdf";

	/**
	 * Nome que � dado para o pdf do codigo de �tica quando salvo no celular.
	 */
	public static final String ARQUIVO_CODIGO_ETICA = "CROSPCodigoEtica.pdf";

	/**
	 * Endere�o do site do crosp que � feita a requisi��o post para adicionar mensagem no fale conosco.
	 */
	public static final String URL_ENVIA_FALE_CONOSCO = "http://www.crosp.org.br/faleconosco/envia.html";

	public static final String SITE_HIMAKER = "http://www.himaker.com.br";

	public static final String SITE_PROGRAMA_INTEGRACAO = "http://www.crosp.org.br/intranet/eventos/cursosxyz/";
	public static final String SITE_PALESTRAS_SEBRAE = "http://www.crosp.org.br/intranet/eventos/cursosebrae/";
	public static final String SITE_FACEBOOK_CROSP = "https://www.facebook.com/CrospOficial";
	public static final String SITE_TWITTER_CROSP = "https://twitter.com/crospoficial";

	/**
	 * Endere�o utilizado para visualizar uma not�cia, deve ser concatenado o id da not�cia ao final do endere�o.
	 */
	public static final String SITE_NOTICIA = "http://www.crosp.org.br/noticia/ver/";

	private Util() {

	}

	/**
	 * @return {@link JSONArray} do conteudo da URL informada.
	 */
	public static JSONArray consultaJSON(String url) {

		String result = null;
		StringBuilder sb = null;
		InputStream is = null;

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		try {
			HttpClient httpclient = new DefaultHttpClient();

			HttpPost httppost = new HttpPost(url);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
			return null;
		}
		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();

			return !result.equals("null\n") ? new JSONArray(result) : null;
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
			return null;
		}
	}

	/**
	 * Efetua o download utilizando o recurso {@link DownloadManager}.
	 * Adiciona o arquivo no {@link Environment#getExternalStorageDirectory()}
	 * 
	 * @see <a href="http://stackoverflow.com/questions/3028306/download-a-file-with-android-and-showing-the-progress-in-a-progressdialog">DownloadManager</a>
	 * 
	 * @param ctx
	 * @param url Endereco de para baixar oa arquivo.
	 * @param title Apresentado na notifica��o do download como t�tulo.
	 * @param description Apresentado na notifica��o do download como descri��o.
	 * @param fileName Nome do arquivo a ser salvo.
	 */
	@SuppressLint("NewApi")
	private static void download(Context ctx, String url, String title,
			String description, String fileName) {

		Toast.makeText(ctx, "Baixando o arquivo", Toast.LENGTH_LONG).show();

		File arquivo = new File(getDocumentsStorageDir(), fileName);

		DownloadManager.Request request = new DownloadManager.Request(
				Uri.parse(url));
		request.setDescription(description);
		request.setTitle(title);
		// in order for this if to run, you must use the android 3.2 to
		// compile
		// your app
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			request.allowScanningByMediaScanner();
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		}

		request.setDestinationUri(Uri.fromFile(arquivo));

		// get download service and enqueue file
		DownloadManager manager = (DownloadManager) ctx
				.getSystemService(Context.DOWNLOAD_SERVICE);
		manager.enqueue(request);
	}

	/**
	 * Download de uma imagem informada atrav�s de uma URL.
	 * 
	 * @param url
	 * @return
	 */
	public static byte[] download(String url) {

		try {
			URL u = new URL(url);
			HttpURLConnection cnn = (HttpURLConnection) u.openConnection();
			cnn.setRequestProperty("Request-Method", "GET");
			cnn.setDoInput(true);
			cnn.setDoOutput(false);
			cnn.connect();
			byte[] bytes = readBytes(cnn.getInputStream());
			cnn.disconnect();
			return bytes;
		} catch (Exception e) {
			Log.e(Util.class.getSimpleName(),
					"N�o foi poss�vel efetuar download da imagem.", e);
		}

		return null;
	}

	/**
	 * Efetua o download de um arquivo, apresenta notificacao com barra de porcentagem. O arquivo � armazendo no external storage directory do celular.
	 * 
	 * @see <a href="http://stackoverflow.com/questions/3028306/download-a-file-with-android-and-showing-the-progress-in-a-progressdialog">DownloadManager</a>
	 * @param ctx
	 * @param url Endere�o URL onde se encontra o arquivo para ser baixado.
	 * @param arquivo Nome que ser� salvo o arquivo no celular.
	 * @param tipoArquivo Tipo do arquivo (utilizado para escolher qual aplicativo deve abrir o arquivo.
	 * @param titulo Titulo apresentado na notifica��o do download.
	 * @param descricao Descri��o utilizada na notifica��o do download.
	 * @param overrightExistintFile Caso true, sobreescreve o arquivo se j� existir, caso false nao faz download e abre o arquivo existente.
	 */
	public static void downloadNotification(Context ctx, String url,
			String arquivo, String tipoArquivo, String titulo,
			String descricao, boolean overrightExistintFile) {

		File file = new File(getDocumentsStorageDir(), arquivo);

		// se deve utilzar o arquivo existente caso ja exista
		if (!overrightExistintFile && file.exists()) {
			Intent target = new Intent(Intent.ACTION_VIEW);

			target.setDataAndType(Uri.fromFile(file), tipoArquivo);
			target.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			ctx.startActivity(target);

		} else {

			// se o arquivo j� existir, remove
			if (file.exists()) {
				file.delete();
			}
			Util.download(ctx, url, titulo, descricao, arquivo);
		}
	}

	/**
	 * Executa o m�todo {@link Util#downloadNotification(Context, String, String, String, String, String)} informando true para o par�metro overrightExistingMethod.
	 */
	public static void downloadNotification(Context ctx, String url,
			String arquivo, String tipoArquivo, String titulo, String descricao) {

		downloadNotification(ctx, url, arquivo, tipoArquivo, titulo, descricao,
				false);
	}

	private static byte[] readBytes(InputStream in) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			byte[] buffer = new byte[1024];
			int len;
			while ((len = in.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
			return bos.toByteArray();
		} finally {
			bos.close();
			in.close();
		}
	}

	/**
	 * Acessa uma url passando os parametros como post.
	 * 
	 * @param urlAddres
	 * @param params
	 * @return True caso a requisi��o foi feita.
	 */
	public static void postRequest(String urlAddres, Map<String, String> params) {

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(urlAddres);

		List<NameValuePair> parameters = new ArrayList<NameValuePair>();

		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, String> mapEntry = iterator.next();
			parameters.add(new BasicNameValuePair(mapEntry.getKey(), mapEntry
					.getValue()));
		}

		try {
			httppost.setEntity(new UrlEncodedFormEntity(parameters));
			HttpResponse response = httpclient.execute(httppost);
		} catch (Exception e) {
			Log.e(Util.class.getSimpleName(),
					"Problema ao tentar fazer requisi��o post.", e);
		}
	}

	/**
	 * Verifica se o mime type informado pode ser aberto por algum app do celular.
	 * 
	 * @see <a href="http://stackoverflow.com/a/2790374">Link</a>
	 */
	public static boolean canDisplay(Context context, String mimeType) {

		PackageManager packageManager = context.getPackageManager();
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		testIntent.setType(mimeType);
		if (packageManager.queryIntentActivities(testIntent,
				PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Caminho do diret�rio p�blico onde os documentos s�o armazenados. Caso o diret�rio n�o exista ele � criado.
	 */
	public static File getDocumentsStorageDir() {

		File dir = new File(Environment.getExternalStorageDirectory(), "crosp");

		if (!dir.mkdirs()) {
			Log.e(Util.class.getSimpleName(),
					"N�o criou diret�rio de download de documentos.");
		}

		return dir;
	}

	public static void openFile(String fileName, Context ctx, String mimeType) {
		Intent install = new Intent(Intent.ACTION_VIEW);
		install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		install.setDataAndType(Uri.fromFile(new File(fileName)), mimeType);
		ctx.startActivity(install);
	}

	/**
	 * Abre a url informada no aplicativo de browser do celular.
	 * 
	 * @param ctx
	 * @param url Endere�o do site a ser aberto no browser (usar http://).
	 */
	public static void openBrowser(Context ctx, String url) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addCategory(Intent.CATEGORY_BROWSABLE);
		intent.setData(Uri.parse(url));
		ctx.startActivity(intent);
	}

}
