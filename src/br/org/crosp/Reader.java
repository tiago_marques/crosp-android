package br.org.crosp;

import android.database.Cursor;

public class Reader {

	private Cursor cursor;

	public Reader(Cursor cursor) {
		this.cursor = cursor;
	}

	public boolean read() {
		return cursor.moveToNext();
	}

	public void close() {
		cursor.close();
	}

	public float getFloat(int columnIndex) {
		return cursor.getFloat(columnIndex);
	}

	public float getFloat(String columnIndex) {
		return cursor.getFloat(cursor.getColumnIndex(columnIndex));
	}

	public int getInt(int columnIndex) {
		return cursor.getInt(columnIndex);
	}

	public int getInt(String columnIndex) {
		return cursor.getInt(cursor.getColumnIndex(columnIndex));
	}

	public String getString(int columnIndex) {
		return cursor.getString(columnIndex);
	}

	public String getString(String columnIndex) {
		return cursor.getString(cursor.getColumnIndex(columnIndex));
	}

	public byte[] getBlob(String columnIndex) {
		return cursor.getBlob(cursor.getColumnIndex(columnIndex));
	}

	public Long getLong(String columnIndex) {
		return cursor.getLong(cursor.getColumnIndex(columnIndex));
	}

	public Cursor getCursor() {
		return cursor;
	}

}
