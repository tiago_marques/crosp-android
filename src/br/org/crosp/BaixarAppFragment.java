package br.org.crosp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import br.org.crosp.noticia.R;

public class BaixarAppFragment extends Fragment {

	private Button btBaixarAppPdf;

	private static final String URL_APP_PDF_READER = "http://play.google.com/store/apps/details?id=com.adobe.reader";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.baxar_pdf_app_fragment,
				container, false);

		btBaixarAppPdf = (Button) rootView.findViewById(R.id.baixar_app_pdf_botao_baixar);
		btBaixarAppPdf.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse(URL_APP_PDF_READER));
				startActivity(intent);
			}
		});

		return rootView;
	}

	@Override
	public void onResume() {
		getActivity().setTitle("Baixar aplicativo");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);

		super.onResume();
	}

}
