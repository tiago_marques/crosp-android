package br.org.crosp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class Database extends SQLiteOpenHelper{

	protected SQLiteDatabase db;
	public Database(Context context, String name, int version){
		super(context, name, null, version);
	}
	public void open(){
		db = getWritableDatabase();
	}
	public Reader query(String sql){
		return new Reader(db.rawQuery(sql, null));
	}
	
	public Reader query(String sql, Object... args ){
		return query(String.format(sql, args));
	}
	
	public void execute(String sql){
		db.execSQL(sql);
	}
	
	public void execute(String format, Object... args){
		execute(String.format(format, args));
	}

	public int scalarInt(String sql, Object... args) {
		int result = 0;
		Reader r = query(sql, args);
		if (r.read()){
			result = r.getInt(0);
		}
			r.close();
			return result;
		}
}
