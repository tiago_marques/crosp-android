package br.org.crosp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import br.org.crosp.noticia.R;
import br.org.crosp.sobreDesenvolvimento.view.SobreDesenvolvimentoActivity;

public class SobreCrospFragment extends Fragment {

	private ImageView imageInfo, phoneIcon, iconFbCrospOficial,
			iconTwCrospOficial;
	private TextView phoneTextView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.sobre_crosp,
				container, false);

		phoneTextView = (TextView) rootView
				.findViewById(R.id.sobrecrospTelefone);
		phoneTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ligar();
			}
		});

		phoneIcon = (ImageView) rootView.findViewById(R.id.sobrecrospPhoneIcon);
		phoneIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ligar();
			}
		});

		imageInfo = (ImageView) rootView
				.findViewById(R.id.sobrecrospInfoImageView);

		imageInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(rootView.getContext(),
						SobreDesenvolvimentoActivity.class));

			}
		});
		
		iconFbCrospOficial = (ImageView) rootView
				.findViewById(R.id.sobrecrospFbImageView);
		iconFbCrospOficial.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Util.openBrowser(getActivity().getApplicationContext(),
						Util.SITE_FACEBOOK_CROSP);
			}
		});

		iconTwCrospOficial = (ImageView) rootView
				.findViewById(R.id.sobrecrospTwImageView);
		iconTwCrospOficial.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Util.openBrowser(getActivity().getApplicationContext(),
						Util.SITE_TWITTER_CROSP);
			}
		});

		return rootView;
	}


	private void ligar() {
		Intent i = new Intent(Intent.ACTION_CALL);
		i.setData(Uri.parse("tel:" + phoneTextView.getText().toString()));
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	@Override
	public void onResume() {
		getActivity().setTitle("O CROSP");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);
		super.onResume();
	}

}
