package br.org.crosp.faleConosco.view;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import br.org.crosp.BreadCrumb;
import br.org.crosp.Util;
import br.org.crosp.noticia.R;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;

public class FaleConoscoFragment extends Fragment implements ValidationListener {

	@Required(order = 1, message = "O nome deve ser informado")
	private EditText etNome;
	@Required(order = 2, message = "O email deve ser informado")
	private EditText etEmail;
	private EditText etTelefone;
	private EditText etNrCrosp;
	private EditText etMensagem;

	private Spinner catProfissaoSpinner;
	private Spinner manifestacaoSpinner;
	private Spinner setorCrospSpinner;

	private View rootView;
	private Validator validator;

	private Button btEnviar;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.faleconoso_fragment, container,
				false);

		validator = new Validator(this);
		validator.setValidationListener(this);

		etNome = (EditText) rootView
				.findViewById(R.id.faleConosco_nomeEditText);
		etEmail = (EditText) rootView
				.findViewById(R.id.faleConosco_emailEditText);
		etTelefone = (EditText) rootView
				.findViewById(R.id.faleConosco_telefoneEditText);
		etNrCrosp = (EditText) rootView
				.findViewById(R.id.faleConosco_nrCrospEditText);
		etMensagem = (EditText) rootView
				.findViewById(R.id.faleConosco_mensagemEditText);

		btEnviar = (Button) rootView
				.findViewById(R.id.fale_conosco_botao_enviar);

		catProfissaoSpinner = (Spinner) rootView
				.findViewById(R.id.faleConosco_catProfissaoSpinner);
		ArrayAdapter<CharSequence> carProfissaoAdapter = ArrayAdapter
				.createFromResource(rootView.getContext(),
						R.array.cat_profissao,
						android.R.layout.simple_spinner_item);
		carProfissaoAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		catProfissaoSpinner.setAdapter(carProfissaoAdapter);

		manifestacaoSpinner = (Spinner) rootView
				.findViewById(R.id.faleConosco_ManifestacaoSpinner);
		ArrayAdapter<CharSequence> manifestacaoAdapter = ArrayAdapter
				.createFromResource(rootView.getContext(),
						R.array.manifestacao,
						android.R.layout.simple_spinner_item);
		manifestacaoAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		manifestacaoSpinner.setAdapter(manifestacaoAdapter);

		setorCrospSpinner = (Spinner) rootView
				.findViewById(R.id.faleConosco_setorCrospSpinner);
		ArrayAdapter<CharSequence> setorCrospAdapter = ArrayAdapter
				.createFromResource(rootView.getContext(), R.array.setoCrosp,
						android.R.layout.simple_spinner_item);
		setorCrospAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		setorCrospSpinner.setAdapter(setorCrospAdapter);

		btEnviar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});

		return rootView;
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {

		String message = failedRule.getFailureMessage();

		if (failedView instanceof EditText) {
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		} else {
			Toast.makeText(rootView.getContext(), message, Toast.LENGTH_SHORT)
					.show();
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void onValidationSucceeded() {
		Toast.makeText(rootView.getContext(), "Enviando mensagem",
				Toast.LENGTH_SHORT).show();

		SendMessage send = new SendMessage();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			send.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			send.execute();
		}
	}

	private void limparCampos() {

		etNome.getText().clear();
		etEmail.getText().clear();
		etTelefone.getText().clear();
		etNrCrosp.getText().clear();
		catProfissaoSpinner.setSelection(0);
		manifestacaoSpinner.setSelection(0);
		setorCrospSpinner.setSelection(0);
		etMensagem.getText().clear();
	}

	/**
	 * Envia a mensagem de fale conosco para o crosp.
	 */
	private class SendMessage extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			Map<String, String> post = new HashMap<String, String>();

			post.put("nome", etNome.getText().toString());
			post.put("email", etEmail.getText().toString());
			post.put("telefone", etTelefone.getText().toString());
			post.put("crosp", etNrCrosp.getText().toString());
			post.put("profissao", categoriaPost());
			post.put("manifestacao", manifestacaoSpinner.getSelectedItem()
					.toString());
			post.put("setor", setorCrospSpinner.getSelectedItem().toString());
			post.put("mensagem", etMensagem.getText().toString());

			Util.postRequest(Util.URL_ENVIA_FALE_CONOSCO, post);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			Toast.makeText(rootView.getContext(), "Mensagem enviada",
					Toast.LENGTH_LONG).show();
			limparCampos();
			super.onPostExecute(result);
		}
	}

	/**
	 * O servidor do crosp recepe como post apenas o codigo da categoria e nao a descricao inteira.
	 * @return
	 */
	private String categoriaPost() {

		String categoria = catProfissaoSpinner.getSelectedItem().toString();
		if (categoria.equals("CD - Cirurgi�o Dentista")) {
			return "CD";
		}

		if (categoria.equals("TPD - T�cnico de Pr�tese Dent�ria")) {
			return "TPD";
		}

		if (categoria.equals("TSB - T�cnico em Sa�de Bucal")) {
			return "TSB";
		}

		if (categoria.equals("ASB - Auxiliar em Sa�de Bucal")) {
			return "ASB";
		}

		if (categoria.equals("APD - Atendente de Pr�tese Dent�ria")) {
			return "APD";
		}

		if (categoria.equals("CL - Cl�nica Odontol�gica")) {
			return "CL";
		}

		if (categoria.equals("LB - Laborat�rio de Pr�tese Dent�ria")) {
			return "LB";
		}

		if (categoria.equals("CIP - Com�rcio e Ind�stria de Produtos")) {
			return "CIP";
		}

		if (categoria.equals("Outros")) {
			return "OUT";
		}

		return null;
	}

	@Override
	public void onResume() {
		getActivity().setTitle("Fale Conosco");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);

		super.onResume();
	}

}
