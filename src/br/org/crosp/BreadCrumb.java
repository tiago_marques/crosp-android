package br.org.crosp;

import java.util.Stack;

import android.app.Application;
import android.support.v4.app.Fragment;

public class BreadCrumb extends Application {

	private Stack<Fragment> lista = new Stack<Fragment>();

	public Stack<Fragment> getLista() {
		return lista;
	}

	public void setLista(Stack<Fragment> lista) {
		this.lista = lista;
	}


}
