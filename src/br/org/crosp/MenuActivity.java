package br.org.crosp;

import java.util.Stack;

import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import br.org.crosp.codigoEtica.sevice.CodigoEticaService;
import br.org.crosp.faleConosco.view.FaleConoscoFragment;
import br.org.crosp.noticia.R;
import br.org.crosp.noticia.service.NoticiaService;
import br.org.crosp.noticia.view.NoticiasFragment;
import br.org.crosp.publicacao.view.BoletinsFragment;
import br.org.crosp.publicacao.view.JornaisFragment;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class MenuActivity extends SherlockFragmentActivity {

	// Declare Variables
	DrawerLayout mDrawerLayout;
	ListView mDrawerList;
	ActionBarDrawerToggle mDrawerToggle;
	MenuListAdapter mMenuAdapter;
	String[] title;
	String[] subtitle;
	int[] icon;
	Fragment initialFragment = new InicialFragment();
	Fragment noticiasFragment = new NoticiasFragment();
	Fragment boletinsFragment = new BoletinsFragment();
	Fragment jornaisFragment = new JornaisFragment();
	Fragment faleConoscoFragment = new FaleConoscoFragment();
	Fragment sobreCROSPFragment = new SobreCrospFragment();

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private CodigoEticaService codEticaService;

	private Stack<Fragment> breadCrumb;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from drawer_main.xml
		setContentView(R.layout.drawer_main);

		breadCrumb = ((BreadCrumb) getApplicationContext()).getLista();

		getSupportActionBar().setIcon(android.R.color.transparent);
		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(getResources().getColor(R.color.barra)));

		// Get the Title
		mTitle = mDrawerTitle = getTitle();

		// Generate title
		title = new String[] { "In�cio", "Not�cias", "Boletins Peri�dicos",
				"CROSP em Not�cia", "Fale Conosco", "C�digo de �tica",
				"Programa de Integra��o", "Palestras SEBRAE", "O CROSP" };

		// Generate icon
		icon = new int[] { R.drawable.action_about, R.drawable.action_settings,
				R.drawable.collections_cloud };

		// Locate DrawerLayout in drawer_main.xml
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// Locate ListView in drawer_main.xml
		mDrawerList = (ListView) findViewById(R.id.listview_drawer);

		// Set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		// Pass string arrays to MenuListAdapter
		mMenuAdapter = new MenuListAdapter(MenuActivity.this, title, icon);

		// Set the MenuListAdapter to the ListView
		mDrawerList.setAdapter(mMenuAdapter);

		// Capture listview menu item click
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// Enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				// TODO Auto-generated method stub
				super.onDrawerClosed(view);
			}

			public void onDrawerOpened(View drawerView) {
				// TODO Auto-generated method stub
				// Set the title on the action when drawer open
				getSupportActionBar().setTitle(mDrawerTitle);
				super.onDrawerOpened(drawerView);
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {

			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);
			}
		}

		return super.onOptionsItemSelected(item);
	}

	// ListView click listener in the navigation drawer
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		// Locate Position
		switch (position) {
		case 0:
			ft.replace(R.id.content_frame, initialFragment);
			break;
		case 1:
			ft.replace(R.id.content_frame, noticiasFragment);
			break;
		case 2:
			ft.replace(R.id.content_frame, boletinsFragment);
			break;
		case 3:
			ft.replace(R.id.content_frame, jornaisFragment);
			break;
		case 4:
			ft.replace(R.id.content_frame, faleConoscoFragment);
			break;
		case 5:
			if (codEticaService == null) {
				codEticaService = new CodigoEticaService(
						getApplicationContext(), getSupportFragmentManager());
			}
			codEticaService.downloadAndOpenCodigoEtica();
			break;
		case 6:
			Util.openBrowser(getApplicationContext(),
					Util.SITE_PROGRAMA_INTEGRACAO);
			break;
		case 7:
			Util.openBrowser(getApplicationContext(),
					Util.SITE_PALESTRAS_SEBRAE);
			break;
		case 8:
			ft.replace(R.id.content_frame, sobreCROSPFragment);
			break;

		}
		ft.commit();
		mDrawerList.setItemChecked(position, true);

		// Get the title followed by the position
		setTitle(title[position]);
		// Close drawer
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (breadCrumb.size() > 1) {
				breadCrumb.pop(); // remove a acti
				if (breadCrumb.size() > 0) {
					FragmentTransaction ft = getSupportFragmentManager()
							.beginTransaction();
					ft.replace(R.id.content_frame, breadCrumb.pop());
					ft.commit();
					return true;
				}
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Quando a aplica��o eh destruida o banco eh limpo.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// pegara novas noticias utilizando o refresh NoticiaService.refresh()
		NoticiaService.apagarNoticias(getApplicationContext());
	}

}
