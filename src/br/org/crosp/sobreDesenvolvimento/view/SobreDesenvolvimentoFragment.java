package br.org.crosp.sobreDesenvolvimento.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.org.crosp.noticia.R;

public class SobreDesenvolvimentoFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.sobre_desenvolvimento,
				container,
				false);


		return rootView;
	}

}
