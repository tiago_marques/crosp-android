package br.org.crosp.sobreDesenvolvimento.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import br.org.crosp.MenuActivity;
import br.org.crosp.Util;
import br.org.crosp.noticia.R;

public class SobreDesenvolvimentoActivity extends Activity {

	private ImageView imageBack, imageLogo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sobre_desenvolvimento);

		imageBack = (ImageView) this
				.findViewById(R.id.sobreDesenvolvimentoBackIcon);
		imageBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				startActivity(new Intent(getApplicationContext(),
						MenuActivity.class));
			}
		});
		
		imageLogo = (ImageView) this.findViewById(R.id.logo_himaker);
		imageLogo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Util.openBrowser(getApplicationContext(), Util.SITE_HIMAKER);
			}
		});
	}

}
