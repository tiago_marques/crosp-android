package br.org.crosp.noticia.model;

import java.util.Date;

public class Noticia implements Comparable<Noticia> {

	private int id;
	private String titulo;
	private String chamada;
	private String conteudo;
	private Date data;
	private byte[] imagemDestaque;
	private byte[] imagem;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getChamada() {
		return chamada;
	}

	public void setChamada(String chamada) {
		this.chamada = chamada;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public byte[] getImagemDestaque() {
		return imagemDestaque;
	}

	public void setImagemDestaque(byte[] imagem) {
		this.imagemDestaque = imagem;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	@Override
	public int compareTo(Noticia another) {
		return another.getData().compareTo(this.getData());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Noticia other = (Noticia) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
