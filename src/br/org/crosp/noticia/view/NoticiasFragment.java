package br.org.crosp.noticia.view;

import java.util.LinkedList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import br.org.crosp.BreadCrumb;
import br.org.crosp.noticia.R;
import br.org.crosp.noticia.model.Noticia;
import br.org.crosp.noticia.service.NoticiaService;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;

public class NoticiasFragment extends Fragment {

	private NoticiaService noticiaService;
	private PullAndLoadListView list;
	private NoticiasAdapter noticiasAdapter;
	private Context ctx;
	private LinkedList<Noticia> noticias;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.lista_noticia_fragment,
				container, false);

		noticiaService = new NoticiaService(rootView.getContext());
		list = (PullAndLoadListView) rootView.findViewById(R.id.lista_noticias);
		ctx = rootView.getContext();

		LoadUltimasNoticias load = new LoadUltimasNoticias();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			load.execute();
		}

		return rootView;
	}

	/**
	 * Tarefa assincrona para consultar as �ltimas {@link Noticia}.
	 * Classe que monta a lista de {@link Noticia} pela primeira vez.
	 */
	private class LoadUltimasNoticias extends
			AsyncTask<Void, Void, LinkedList<Noticia>> {

		@Override
		protected LinkedList<Noticia> doInBackground(Void... params) {

			noticias = new LinkedList<Noticia>(noticiaService.ultimasNoticias());
			return noticias;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(LinkedList<Noticia> result) {


			noticiasAdapter = new NoticiasAdapter(ctx, result);
			list.setAdapter(noticiasAdapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					Noticia n = (Noticia) parent.getAdapter().getItem(position);

					Bundle bundle = new Bundle();
					bundle.putInt("id", n.getId());

					NoticiaFragment noticiaFragment = new NoticiaFragment();
					noticiaFragment.setArguments(bundle);

					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.content_frame, noticiaFragment)
							.commit();
				}
			});

			// adiciona acao para dar refresh na lista
			list.setOnRefreshListener(new OnRefreshListener() {
				public void onRefresh() {

					RefreshNoticias refresh = new RefreshNoticias();
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						refresh.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					} else {
						refresh.execute();
					}
				}
			});

			// adiciona acao para carregar noticias antigas
			list.setOnLoadMoreListener(new OnLoadMoreListener() {
				public void onLoadMore() {
					LoadMoreNoticias load = new LoadMoreNoticias();
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					} else {
						load.execute();
					}
				}
			});

			super.onPostExecute(result);
		}
	}

	/**
	 * Tarefa para fazer consultar se existe novas not�cias.
	 */
	private class RefreshNoticias extends
			AsyncTask<Void, Void, LinkedList<Noticia>> {

		@Override
		protected LinkedList<Noticia> doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}

			return new LinkedList<Noticia>(noticiaService.refresh());
		}

		@Override
		protected void onPostExecute(LinkedList<Noticia> result) {
			
			noticias.addAll(0, result);
			noticiasAdapter.notifyDataSetChanged();
			list.onRefreshComplete();
			super.onPostExecute(noticias);
		}

		@Override
		protected void onCancelled() {
			list.onRefreshComplete();
		}
	}

	/**
	 * Tarefa para fazer consultar noticias antigas.
	 */
	private class LoadMoreNoticias extends
			AsyncTask<Void, Void, LinkedList<Noticia>> {

		@Override
		protected LinkedList<Noticia> doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}

			return new LinkedList<Noticia>(noticiaService.loadMore());
		}

		@Override
		protected void onPostExecute(LinkedList<Noticia> result) {

			noticias.addAll(noticias.size(), result);
			noticiasAdapter.notifyDataSetChanged();
			((PullAndLoadListView) list).onLoadMoreComplete();

			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
			// Notify the loading more operation has finished
			((PullAndLoadListView) list).onLoadMoreComplete();
		}
	}

	@Override
	public void onResume() {
		getActivity().setTitle("Not�cias");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);
		super.onResume();
	}

}
