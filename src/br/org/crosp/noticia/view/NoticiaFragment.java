package br.org.crosp.noticia.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import br.org.crosp.BreadCrumb;
import br.org.crosp.Util;
import br.org.crosp.noticia.R;
import br.org.crosp.noticia.model.Noticia;
import br.org.crosp.noticia.service.NoticiaService;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.widget.ShareActionProvider;

public class NoticiaFragment extends SherlockFragment {

	private NoticiaService noticiaService;
	private TextView titulo;
	private View rootView;

	private ShareActionProvider mShareActionProvider;

	@SuppressLint("NewApi")
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.noticia, container, false);

		noticiaService = new NoticiaService(rootView.getContext());

		LoadNoticia load = new LoadNoticia();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			load.execute();
		}

		// http://stackoverflow.com/questions/20226897/oncreateoptionsmenu-not-called-in-fragment
		setHasOptionsMenu(true);

		return rootView;
	}

	private class LoadNoticia extends AsyncTask<Void, Void, Noticia> {

		private int id;
		private ImageView image;

		@Override
		protected Noticia doInBackground(Void... args) {

			id = getArguments().getInt("id");
			return noticiaService.noticia(id);
		}

		@Override
		protected void onPostExecute(Noticia noticia) {
			titulo = (TextView) rootView.findViewById(R.id.titulonoticia);
			titulo.setText(noticia.getTitulo());

			WebView webView = (WebView) rootView
					.findViewById(R.id.conteudonoticia);

			// replace foi feito, pois no momento em que a noticia � cadastrada
			// (site do crosp) no momento em que o texto chega no limite de
			// colunas do editor a linha � quebrada, por�m o espa�o n�o �
			// digitado isso faz com que a as duas palavras pare
			String conteudoNoticia = noticia.getConteudo().replace("\n", "\n ");

			// artigo que utilizado para funcionar o zoom nas versoes mais
			// antigas http://www.johntvolk.com/?p=1
			webView.loadData(
					solveBug("  <html><head><meta name='viewport' http-equiv='content-type' content='text/hmtl, initial-scale=1.0,minimum-scale=1.0, maximun-scale=10.0' charset='utf-8' /></head>  <body style='text-align:justify; background-color: #E3D9CA;'>"
							+ "<article style='clear:both'>"
							+ conteudoNoticia
							+ "</article> </body></html>"),
					"text/html; charset=utf-8", "UTF-8");

			webView.getSettings().setUseWideViewPort(true);
			webView.getSettings().setLoadWithOverviewMode(true);
			// webView.setInitialScale(100);
			webView.getSettings().setBuiltInZoomControls(true);

			image = (ImageView) rootView.findViewById(R.id.imagem_notica);
			byte[] imageInBytes = noticia.getImagem();
			if (imageInBytes != null) {
				image.setImageBitmap(BitmapFactory.decodeByteArray(
						imageInBytes, 0, imageInBytes.length));
			} else {
				image.setVisibility(View.GONE);
			}

			TextView data = (TextView) rootView.findViewById(R.id.datanoticia);
			Date dataNoticia = noticia.getData();
			if (dataNoticia != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
				data.setText(sdf.format(dataNoticia));
			}

		}
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.share_menu, menu);
		//
		// /** Getting the actionprovider associated with the menu item whose id
		// is
		// share */
		mShareActionProvider = (ShareActionProvider) menu.findItem(R.id.share)
				.getActionProvider();

		// /** Getting the target intent */
		Intent intent = getDefaultShareIntent();

		// /** Setting a share intent */
		if (intent != null)
			mShareActionProvider.setShareIntent(intent);

	}

	/** Returns a share intent */
	private Intent getDefaultShareIntent() {

		int id = getArguments().getInt("id");

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, Util.SITE_NOTICIA + id);

		return intent;
	}

	/**
	 * Utilizado para resolver o bug de carregar caracteres especiais no webviewer.
	 * 
	 * @see http://stackoverflow.com/questions/12332929/webview-webpage-not-available-but-i-load-it-from-an-html-string
	 * @see https://code.google.com/p/android/issues/detail?id=1733
	 */
	private String solveBug(String data) {
		StringBuilder sb = new StringBuilder(data.length() + 100);
		char[] dataChars = data.toCharArray();

		for (int i = 0; i < dataChars.length; i++) {
			char ch = data.charAt(i);
			switch (ch) {
			case '%':
				sb.append("%25");
				break;
			case '\'':
				sb.append("%27");
				break;
			case '#':
				sb.append("%23");
				break;
			default:
				sb.append(ch);
				break;
			}
		}

		return sb.toString();
	}

	@Override
	public void onResume() {
		getActivity().setTitle("Not�cia");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);

		super.onResume();
	}

}
