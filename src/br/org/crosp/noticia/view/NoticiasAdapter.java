package br.org.crosp.noticia.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.org.crosp.noticia.R;
import br.org.crosp.noticia.model.Noticia;

public class NoticiasAdapter extends ArrayAdapter<Noticia> {

	private static final int layoutResourceId = R.layout.noticias_item;
	private LayoutInflater inflater;

	public NoticiasAdapter(Context context, List<Noticia> objects) {
		super(context, layoutResourceId, objects);
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		convertView = inflater.inflate(layoutResourceId, null);

		TextView texto = (TextView) convertView
				.findViewById(R.id.titulochamada);
		TextView data = (TextView) convertView.findViewById(R.id.datanoticia);

		Noticia n = getItem(position);

		texto.setText(n.getTitulo());

		Date dataNoticia = n.getData();
		if (dataNoticia != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:mm");
			data.setText(sdf.format(dataNoticia));
		}

		return convertView;
	}

}
