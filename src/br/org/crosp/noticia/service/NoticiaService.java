package br.org.crosp.noticia.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import br.org.crosp.MenuActivity;
import br.org.crosp.Reader;
import br.org.crosp.Repositorio;
import br.org.crosp.Util;
import br.org.crosp.noticia.model.Noticia;

public class NoticiaService {

	private Repositorio db;

	/**
	 * Quantidade de not�cias que devem ser consultadas quando o aplicativo �
	 * carregado.
	 */
	private static final int LIMIT = 10;

	/**
	 * Quantidade de not�cias que devem ser consultadas quando o usu�rio solicita carregar noticias mais antigas.
	 */
	private static final int LIMIT_LOAD_MORE = 5;

	public NoticiaService(Context ctx) {
		this.db = new Repositorio(ctx);
	}

	/**
	 * Retorna as ultimas {@link Noticia} cadastradas no site do CROSP, seguindo
	 * a seguinte regra.
	 * 
	 * 1. Consulta novas noticias no site caso n�o houver noticia no banco, caso
	 * contr�rio retorna as cadastradas no banco
	 * 
	 * 2. Quando n�o houver not�cias no banco, consulta as ultimas cadastradas
	 * no site, no m�ximo ( {@link NoticiaService#LIMIT} )
	 * 		2.1 Adiciona as noticias contultadas do site no banco local
	 * 		2.2 Consulta noticias no banco local e retorna.
	 * 
	 * 
	 * @return
	 */
	public List<Noticia> ultimasNoticias() {

		// se n�o houver noticia cadastrada no banco local
		if (qtdNoticias() == 0) {
			// consulta no site
			List<Noticia> noticias = noticiasFromJSON();

			// adicionano banco local
			insert(noticias);
		}

		// consulta no banco local e retorna
		return noticiasFromBanco();
	}

	/**
	 * Consulta uma {@link Noticia} e retorna segundo a seguinte regra.
	 * 
	 * 1. Consulta a noticia de id informado no site do crosp.
	 * 2. Atualiza as informa��es da noticia no banco local
	 * 3. Consulta e retorna a not�cia do banco local.
	 * 
	 * 
	 * @param idNoticia
	 * @return {@link Noticia} do id informado.
	 */
	public Noticia noticia(int idNoticia) {

		// consulta a noticia no site do crosp
		Noticia noticia = noticiaFromJSON(idNoticia);

		// se n�o existir a noticia
		if (noticia == null) {
			return null;
		}

		// atualiza as informacoes no banco local
		update(noticia);

		// consulta e retorna a noticia do banco local
		return noticiaFromBanco(idNoticia);
	}

	/**
	 * Consulta a lista de {@link Noticia} no site do crosp que possuem o id maior que o id da ultima noticia 
	 * cadastrada no banco local. Adiciona as noticias consultadas no banco local e as retorna.
	 */
	public List<Noticia> refresh() {

		// consulta noticias no site crosp
		List<Noticia> noticias = noticiasNovasFromJSON(ultimaNoticia());

		// adiciona as noticias no banco local
		insert(noticias);
		return noticias;
	}

	/**
	 * Consulta lista de {@link Noticia} no site do crosp com id menor que a noticia de menor id do banco local.
	 * Adiciona as noticias consultadas no banco local e retorna as mesmas.
	 */
	public List<Noticia> loadMore() {

		// consulta as noticias no crosp
		List<Noticia> noticias = noticiasAntigasFromJSON(primeirNoticia());

		// adiciona no banco local
		insert(noticias);

		// retorna as noticias consultadas
		return noticias;
	}

	/**
	 * Remove as {@link Noticia} do banco. As {@link Noticia} s�o removidas no banco quando a aplica��o � destru�da.
	 * 
	 * @see MenuActivity#onDestroy()
	 * 
	 * @param ctx
	 */
	public static void apagarNoticias(Context ctx) {

		Repositorio db = new Repositorio(ctx);
		db.open();
		db.execute("DELETE FROM noticia");
		db.close();
	}

	private Noticia noticiaFromBanco(int id) {

		db.open();

		Reader reader = db.query("SELECT * FROM noticia WHERE id = %d", id);
		reader.read();

		Noticia retorno = toNoticia(reader);
		db.close();

		return retorno;
	}

	/**
	 * @return Lista de {@link Noticia} cadastradas no banco.
	 */
	private List<Noticia> noticiasFromBanco() {

		db.open();
		Reader reader = db.query("SELECT * FROM noticia");
		List<Noticia> noticias = new ArrayList<Noticia>();
		while (reader.read()) {
			noticias.add(toNoticia(reader));
		}

		db.close();
		Collections.sort(noticias);
		return noticias;
	}

	/**
	 * @return Lista de {@link Noticia} consultadas no site utilizando
	 *         {@link JSONArray}, o limite de not�cias consultadas eh definido
	 *         em {@link NoticiaService#LIMIT}.
	 */
	private List<Noticia> noticiasFromJSON() {

		JSONArray jArray = Util.consultaJSON(Util.URL_SERVICE
				+ "noticiasByLimit.php?limit=" + LIMIT);
		return jsonNoticiasToNoticias(jArray);
	}

	/**
	 * @return Lista de {@link Noticia} com id maior que o informado consultadas no site utilizando
	 *         {@link JSONArray}.
	 */
	private List<Noticia> noticiasNovasFromJSON(int idNoticia) {

		JSONArray jArray = Util.consultaJSON(Util.URL_SERVICE
				+ "noticiasByLimit.php?idUltima=" + idNoticia);
		return jsonNoticiasToNoticias(jArray);
	}

	/**
	 * @return Lista de {@link Noticia} com data maior que a informada consultadas no site utilizando
	 *         {@link JSONArray}. Utiliza o {@link NoticiaService#LIMIT_LOAD_MORE} para limitar a quantidade de noticias consultadas.
	 */
	private List<Noticia> noticiasAntigasFromJSON(Date data) {
		JSONArray jArray = Util.consultaJSON(Util.URL_SERVICE
				+ "noticiasByLimit.php?limit=" + LIMIT_LOAD_MORE + "&data="
				+ data.getTime() / 1000);
		return jsonNoticiasToNoticias(jArray);
	}

	/**
	 * @param idNoticia
	 * @return Not�cia consultada no site.
	 */
	private Noticia noticiaFromJSON(int idNoticia) {

		JSONArray jArray = Util.consultaJSON(Util.URL_SERVICE
				+ "noticiaById.php?id=" + idNoticia);
		try {
			return jsonNoticiaToNoticia(jArray.getJSONObject(0));
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Adiciona no banco a lista de {@link Noticia} informada.
	 * 
	 * @param noticias
	 */
	private void insert(List<Noticia> noticias) {

		db.open();
		String query = "INSERT INTO noticia (id, titulo, data, imagem_destaque) VALUES (?, ?, ?, ?)";

		for (Noticia noticia : noticias) {
			try {

				SQLiteStatement prepStmt = db.getWritableDatabase()
						.compileStatement(query);

				prepStmt.bindLong(1, noticia.getId());
				prepStmt.bindString(2, noticia.getTitulo());
				prepStmt.bindLong(3, noticia.getData().getTime());
				if (noticia.getImagemDestaque() != null
						&& noticia.getImagemDestaque().length != 0) {
					prepStmt.bindBlob(4, noticia.getImagemDestaque());
				} else {
					prepStmt.bindNull(4);
				}
				prepStmt.execute();

			} catch (Exception e) {
				Log.e(NoticiaService.class.getSimpleName(),
						"Problema ao tentar inserir not�cia:  " + e);
			}
		}

		db.close();
	}

	/**
	 * @return Quantidade de {@link Noticia} cadastradas no banco local.
	 */
	private int qtdNoticias() {

		db.open();
		int qtd = db.scalarInt("SELECT COUNT(*) FROM noticia");
		db.close();
		return qtd;
	}

	/**
	 * @return Identificador da �ltima (mais recente) {@link Noticia} cadastrada.
	 */
	private int ultimaNoticia() {

		db.open();
		int id = db.scalarInt("SELECT MAX(id) FROM noticia");
		db.close();
		return id;
	}

	/**
	 * @return Identificador da primeira (mais antiga) {@link Noticia} cadastrada baseada na data.
	 */
	private Date primeirNoticia() {

		db.open();
		Reader r = db.query("SELECT MIN(data) AS data FROM noticia");
		r.getCursor().moveToFirst();
		db.close();
		return new Date(r.getLong("data"));
	}

	private static Noticia toNoticia(Reader reader) {

		Noticia noticia = new Noticia();
		noticia.setId(reader.getInt("id"));
		noticia.setTitulo(reader.getString("titulo"));
		noticia.setImagem(reader.getBlob("imagem"));
		noticia.setData(new Date(reader.getLong("data")));
		noticia.setConteudo(reader.getString("conteudo"));
		return noticia;
	}

	private void update(Noticia noticia) {

		db.open();
		String query = "UPDATE noticia SET conteudo =  ?, imagem = ? WHERE id = ?";
		SQLiteStatement sqlStmt = db.getWritableDatabase().compileStatement(
				query);
		sqlStmt.bindString(1, noticia.getConteudo());
		sqlStmt.bindLong(3, noticia.getId());

		byte[] imagem = noticia.getImagem();
		if (imagem != null) {
			sqlStmt.bindBlob(2, imagem);
		} else {
			sqlStmt.bindNull(2);
		}

		sqlStmt.execute();

		db.close();
	}

	/**
	 * Transforma um ojeto {@link JSONObject} em {@link Noticia}
	 * 
	 * @param json
	 */
	private static List<Noticia> jsonNoticiasToNoticias(JSONArray jArray) {

		List<Noticia> noticias = new ArrayList<Noticia>();
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				try {
					noticias.add(jsonNoticiaToNoticia(jArray.getJSONObject(i)));
				} catch (JSONException e) {
					Log.e(NoticiaService.class.getSimpleName(),
							"Erro ao tentar carregar not�cia do site CROSP.", e);
				}
			}
		}

		return noticias;
	}

	/**
	 * Transforma um ojeto {@link JSONObject} retornado pelo servico
	 * {@link Util#consultaJSON(String)} para objeto {@link Noticia}
	 * 
	 * @param json
	 */
	private static Noticia jsonNoticiaToNoticia(JSONObject json) {

		Noticia noticia = new Noticia();
		try {

			noticia.setId(json.getInt("id"));

			if (!json.isNull("titulo")) {
				noticia.setTitulo(json.getString("titulo"));
			}

			if (!json.isNull("data")) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd H:mm:ss");
					noticia.setData(sdf.parse(json.getString("data")));
				} catch (ParseException e) {
					Log.e(NoticiaService.class.getSimpleName(),
							"Data da not�cia n�o est� no formato correto (yyyy-MM-dd H:mm:ss)",
							e);
				}
			}

			if (!json.isNull("texto")) {
				noticia.setConteudo(json.getString("texto"));
			}

			if (!json.isNull("imagem")) {
				noticia.setImagem(downloadImage(json.getString("imagem")));
			}
		} catch (JSONException e) {
			Log.e(NoticiaService.class.getSimpleName(),
					"Problema ao carregar a noticia do site." + e.getMessage());
		}

		return noticia;
	}

	private static byte[] downloadImage(String nomeImagem) {

		if (nomeImagem != null && nomeImagem.length() > 0) {
			String urlImagem = "http://www.crosp.org.br/uploads/noticia/"
					+ nomeImagem;
			return Util.download(urlImagem);
		}

		return null;
	}
}
