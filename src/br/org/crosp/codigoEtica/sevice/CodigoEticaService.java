package br.org.crosp.codigoEtica.sevice;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentManager;
import br.org.crosp.BaixarAppFragment;
import br.org.crosp.Util;
import br.org.crosp.noticia.R;

public class CodigoEticaService {

	private Context ctx;
	private FragmentManager fragmentManager;

	public CodigoEticaService(Context ctx, FragmentManager fragmentManager) {
		this.ctx = ctx;
		this.fragmentManager = fragmentManager;
	}

	BroadcastReceiver onComplete = new BroadcastReceiver() {
		public void onReceive(Context ctx, Intent intent) {

			Util.openFile(Util.getDocumentsStorageDir() + "/"
					+ Util.ARQUIVO_CODIGO_ETICA, ctx, "application/pdf");
			ctx.unregisterReceiver(onComplete);
		}
	};

	/**
	 * Evetua o download e apresenta o pdf doc�digo de etica.
	 */
	public void downloadAndOpenCodigoEtica() {

		if (!Util.canDisplay(ctx, "application/pdf")) {
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, new BaixarAppFragment())
					.commit();
		} else {

			// registra o receiver para abrir o pdf assim q o download
			// for finalizado.
			ctx.registerReceiver(onComplete, new IntentFilter(
					DownloadManager.ACTION_DOWNLOAD_COMPLETE));

			Util.downloadNotification(ctx, Util.URL_CODIGO_ETICA,
					Util.ARQUIVO_CODIGO_ETICA, "application/pdf",
					"C�digo de �tica do CROSP", null, true);
		}
	}
}
