package br.org.crosp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.org.crosp.noticia.R;

/**
 * Tela inicial do app.
 * 
 * @author Tiago Wanke Marques
 */
public class InicialFragment extends Fragment {


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.inicial_fragment,
				container, false);

		return rootView;
	}

	@Override
	public void onResume() {
		getActivity().setTitle("CROSP");
		((BreadCrumb) getActivity().getApplicationContext()).getLista().push(
				this);

		super.onResume();
	}

}
